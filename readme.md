# Hero Responsive Carousel

* responsive "header carousel"

# Install

Recommended installation is via Bower.

Add `"hero-responsive-carousel": "git@bitbucket.org:artfocus/hero-responsive-carousel.git"`
into your project's dependencies and run `bower install`.

## Basic usage

HTML markup:

    <div class="hero"> <!-- root node wraps main <ul> -->
        <ul>
            <li>
                <img src="/slide-1.jpg" class="responsive" alt="slide 1" />
            </li>
            <li>
                <img src="/slide-2.jpg" class="responsive" alt="slide 2" />
            </li>
            <li>
                <img src="/slide-3.jpg" class="responsive" alt="slide 3" />
            </li>
        </ul>
    </div>

You should redefine some styles, eg.:
    
    .hero {
        height: 550px; // you should overwrite it with expected height
    
        ul {
            li {
                width: @screen-xs-max;
    
                @media @sm {
                    width: @screen-sm-min;
                }
    
                @media @md {
                    width: @screen-md-min;
                }
    
                @media @lg {
                    width: @screen-lg-min;
                }
            }
        }
    }

And load the plugin:

    $(window).load(function() {
        $('.hero').heroCarousel();
    });
