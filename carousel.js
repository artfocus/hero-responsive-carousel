/**
 * Hero Responsive Carousel
 *
 * @copyright (c) 2014 Jaroslav Hranička
 * @copyright (c) 2011 Paul Welsh
 *
 * Based on Paul Welsh's original "Hero Carousel v1.3"
 * (http://www.paulwelsh.info/jquery-plugins/hero-carousel/)
 *
 * Dual licensed under the MIT and GPL licenses:
 * * http://www.opensource.org/licenses/mit-license.php
 * * http://www.gnu.org/licenses/gpl.html
 */
$.fn.heroCarousel = function (options) {
	options = $.extend({
		animationSpeed: 1000,
		navigation: true,
		responsive: true,
		easing: '',
		timeout: 5000,
		pauseOnHover: true,
		currentClass: 'current',
		onLoad: function () {
		},
		onStart: function () {
		},
		onComplete: function () {
		}
	}, options);

	return this.each(function () {
		var outer = $(this);
		var carousel = outer.find('> ul');
		var elements;

		var currentItem = 1;
		var paused = false;
		var animating = false;
		var resizeThrottle;
		var timeoutInterval;

		var childWidth;
		var childHeight;

		var carouselWidth;
		var carouselMarginLeft;

		var carouselNav;
		var carouselNavLink;

		function init() {
			loadElements();

			if (options.timeout > 0 && options.pauseOnHover) {
				carousel.hover(function () {
					paused = true;
				}, function () {
					paused = false;
				});
			}

			if (options.responsive) {
				clearTimeout(resizeThrottle);
				resizeThrottle = setTimeout(function () {
					$(window).resize(refreshDimensions);
				}, 200);
			}

			if (options.navigation && elements.length > 1) {
				loadNavigation();
			}

			refreshDimensions();
			refreshInterval();
			outer.addClass('loaded');
		}

		function loadElements () {
			refreshElements();
			var elementsCount = elements.length;

			var first = elements.filter(':first').addClass(options.currentClass);
			var last = elements.filter(':last');

			// move last before first
			first.before(last);

			if (elementsCount === 2) {
				first.after(first.clone());
				first.after(last.clone());
			}

			refreshElements();
		}

		function loadNavigation() {
			carouselNav = $('<div class="hero-carousel-nav"><a class="prev" href="#"></a><a class="next" href="#"></a></div>');
			carouselNavLink = carouselNav.find('a');

			carouselNav.appendTo(outer);

			carouselNavLink.data('disabled', false).click(function () {
				var navItem = $(this);

				if (navItem.data('disabled') !== false) {
					return false;
				}

				navItem.data('disabled', true);
				navigate(navItem.hasClass('prev'));

				setTimeout(function () {
					navItem.data('disabled', false);
				}, options.animationSpeed + 200);

				return false;
			});
		}

		function refreshDimensions() {
			if (animating) {
				setTimeout(refreshDimensions, 50);
				return;
			}

			childWidth = elements.width();
			childHeight = elements.height();
			var firstChildWidth = (elements.length > 1) ? childWidth : 0;

			carouselWidth = Math.round(childWidth * carousel.children().length);
			carouselMarginLeft = '-' + Math.round(firstChildWidth + Math.round(childWidth / 2)) + 'px';

			carousel.css({
				position: 'relative',
				overflow: 'hidden',
				left: '50%',
				top: 0,
				width: carouselWidth,
				height: childHeight,
				marginLeft: carouselMarginLeft
			});

			if (carouselNav) {
				carouselNav.css({
					position: 'absolute',
					top: 0,
					width: outer.width(),
					height: carousel.height()
				});

				carouselNavLink.css({
					position: 'relative',
					float: 'left',
					width: (outer.width() - childWidth) / 2,
					height: carousel.height()
				});

				carouselNavLink.eq(1).css({
					float: 'right'
				});
			}
		}

		function refreshElements() {
			elements = carousel.find('> li');
			return elements;
		}

		function autoSlide() {
			if (!paused) {
				navigate();
			}
		}

		function refreshInterval() {
			if (!options.timeout || elements.length < 2) {
				return;
			}

			window.clearInterval(timeoutInterval);
			timeoutInterval = window.setInterval(autoSlide, options.timeout);
		}

		function navigate(backwards) {
			if (animating) {
				return;
			}

			options.onStart(carousel, elements.eq(currentItem), options);

			if (backwards) {
				animateItem(elements.filter(':last'), 'previous');
			} else {
				animateItem(elements.filter(':first'), 'next');
			}

			refreshInterval();
		}

		function animateItem(object, direction) {
			var carouselPosLeft = parseFloat(carousel.position().left);
			var clone = object.clone();
			var plusOrMinus;

			if (direction === 'previous') {
				carousel.prepend(clone);
				carouselPosLeft -= childWidth;
				plusOrMinus = '+=';
			} else {
				carousel.append(clone);
				plusOrMinus = '-=';
			}

			refreshElements();

			carousel.css({
				left: carouselPosLeft,
				width: Math.round(carouselWidth + childWidth)
			}).animate({
				left: plusOrMinus + childWidth
			}, options.animationSpeed, options.easing, function () {
				carousel.css({
					left: '50%',
					width: carouselWidth
				});

				object.remove();
				finishCarousel();
			});
		}

		function finishCarousel() {
			refreshElements();
			elements.removeClass(options.currentClass).eq(currentItem).addClass(options.currentClass);
			animating = false;

			options.onComplete(carousel, elements.eq(currentItem), options);
		}

		init();
		options.onLoad(carousel, carousel.children().eq(currentItem), options);
	});
};
